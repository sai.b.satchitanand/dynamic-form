import { FormProps } from '@rjsf/core'

export type DynamicFormProps = FormProps<unknown>
