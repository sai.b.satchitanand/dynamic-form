import React from 'react'
import Form from '@rjsf/material-ui'
import { DynamicFormProps } from './types'

export const DynamicForm: React.FC<DynamicFormProps> = (props) => {
  return <Form {...props} />
}
