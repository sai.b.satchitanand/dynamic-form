# Dynamic form

## Demo
https://dynamic-form.vercel.app/

Bootstraped with create-next-app and uses following technologies:

- [react-json-schema-form](https://react-jsonschema-form.readthedocs.io/)
- [Typescript](https://www.typescriptlang.org/)
- Linting with [ESLint](https://eslint.org/)
- Formatting with [Prettier](https://prettier.io/)

## Run locally

```bash
yarn
yarn dev
```

## Schema
- Generally schema is defined over an API, but here it's defined in [page](https://gitlab.com/sai.b.satchitanand/dynamic-form/-/blob/master/pages/index.tsx)