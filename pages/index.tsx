import dynamic from 'next/dynamic'
import { DynamicFormProps } from '../components/DynamicForm/types'

const DynamicForm = dynamic<DynamicFormProps>(() => import('../components/DynamicForm').then((m) => m.DynamicForm), {
  ssr: false,
})

const schema: DynamicFormProps['schema'] = {
  type: 'object',
  title: 'Signup Form',

  properties: {
    firstName: {
      type: 'string',
      title: 'First Name',
    },
    lastName: {
      type: 'string',
      title: 'Last Name',
    },
    email: {
      type: 'string',
      format: 'email',
      title: 'Email',
    },
    password: {
      type: 'string',
      format: 'password',
      title: 'Password',
    },
    termsAndCondition: {
      type: 'boolean',
      title: 'Terms and Conditions and Privacy Policy',
    },
  },

  required: ['firstName', 'lastName', 'email', 'password', 'termsAndCondition'],
}

const errMessages: Record<string, Record<string, string>> = {
  firstName: {
    required: 'First Name is Required',
  },
  lastName: {
    required: 'Last Name is Required',
  },
  email: {
    required: 'Email is Required',
    format: 'Email should be correct format which is abc@example.com',
  },
  password: {
    required: 'Password is required',
  },
  termsAndCondition: {
    required: 'Required',
  },
}

export const Home = (): JSX.Element => (
  <div className="container">
    <DynamicForm
      schema={schema}
      noHtml5Validate
      showErrorList={false}
      transformErrors={(errors) => {
        return errors.map((error) => {
          error.message = errMessages[error.property.substr(1)][error.name]
          return error
        })
      }}
    />
    <style jsx>{`
      .container {
        display: grid;
        grid-template-columns:
          1fr
          min(65ch, 100%)
          1fr;
      }
      .container > :global(*) {
        grid-column: 2;
      }
    `}</style>

    <style jsx global>{`
      html,
      body {
        padding: 0;
        margin: 0;
        font-family: Roboto, sans-serif;
      }

      * {
        box-sizing: border-box;
      }
    `}</style>
  </div>
)

export default Home
